# Contact Info

Subscribe the [Notifications Service News](https://notifications.web.cern.ch/channels/1f0155ad-d7f9-431e-9b7f-a2128dd2cec2/notifications) channel to stay up to date with new features and incidents.

Report any issues or questions at our Mattermost channel or Service Now:

- Mattermost channel: [~Notifications](https://mattermost.web.cern.ch/it-dep/channels/notifications)

- Service Now FE: [Notifications](https://cern.service-now.com/service-portal?id=functional_element&name=notifications)