# Notification Lifecycle
## Archival
By default, Notifications are deleted after 13 months from the Notifications Service, they're no longer in the Notifications Service storage and are not accessible through the Notifications UI or API. Channels owners can turn on the Archive feature, meaning notifications are archived to the Notification Archives after 13 months, and stay accessible.

Be aware that archival can be enabled or disabled but removal from the Notifications Service can't be prevented.

Targeted notifications, both for users and groups, are never archived for privacy reasons. These notifications will be deleted after 13 months from our service.

## Configuration
Configuration can be done at the bottom of the Edit Channel section of the Manage channel panel.

## Access
Channel archived is accessible from the Channel Notifications page.
