# Notifications Service API

The Notifications Service API is an OIDC protected API that enables services to manage Channels, Notifications, Devices and more. You may want to use it, for example to programmatically create Channels and send Notifications.

# Swagger Documentation

You can test the Notifications Service API using the Swagger Interface Through the API you can manage different resources such as channels and notifications. 

To avoid accidentally spamming user while preparing your integration we prepared a QA environment in which you can simulate a real integration. 
In this environment services and individuals can get familiar with the API and target real users without fear of actually reaching them. 

All users need to opt-in to an e-group to be allowed to receive notifications in this environment. 
Users should subscribe to the `notifications-qa-users` e-group to have access to receive notifications in QA.

[Swagger UI](https://notifications-qa.web.cern.ch/swagger) is available on the QA environment. (Intranet only).


# API Endpoints

- QA: https://notifications-qa.web.cern.ch/api
- Production: https://notifications.web.cern.ch/api

## Channel API Key

A Channel API Key can be generated to used in the send notification API.
Once generated, don't forget to enable this permission: `Allow scripts with the API Key to send notifications` in the UI.

If you need to programmatically send notifications, use can use the API Key in the HTTP header when calling the API with POST and a Notification object as below:

```
POST https://notifications.web.cern.ch/api/notifications
Content-Type: application/json
Authorization: Bearer '<api-key>'
{
  "target": "channel id that you can find in your browser url bar",
  "summary": "Notification title",
  "priority": "LOW|NORMAL|IMPORTANT",
  "body": "Notification content"
}
```