# Service mandate

#### Sending Notifications

- By Mail
    - From: owner, member or EGroup
    - To: channels
- By Website
    - From: owner, member
    - To: channels or direct channel members
    - Instant or scheduled
- By API
    - From: owner, member
    - To: channels or direct channel members
    - Instant or scheduled

#### Receiving Notifications

- By Mail
    - Live mail (Default)
    - Digest mail (daily, weekly, monthly)
- By Push Notifications
    - Browser based
    - Desktop and smartphone
- By Mattermost
    - Via Direct Messages
- None
    - View on Notifications Website
- More connectors can/will be added

#### Delivery process

Based on User Preferences

- Global or per Channel
- Target device (mail, browser push, mattermost, etc.)
- Preference time (when preference applies)
- Notification type (direct or global)
- Notification priority
- Frequency
    - Live, Digest (Daily, Weekly, Monthly)
- Mute option:
    - Global or per Channel
    - Permanent or scheduled
