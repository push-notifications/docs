# Notifications Service documentation

### **Objectives**

- **Empower users to choose how to consume information**
- Enhance and complement communication methods
- Improve message trust
- Reduce the mailbox stress
    - Unwanted Mail floods, mailing lists overload
    - Mattermost channels overload
- Not a new message system
    - A **Hub** between existing message systems
    - Provide a common sending point
- avoid replication of code, costs and efforts by avoiding multiple implementations of notification sending via email, mattermost, etc  

### **The motivation**

**Everything goes through mail and egroups**

<figure markdown>
  ![Motivation](static/index/motivation.png){ width="800" }
</figure>

**With no or little user control**

<figure markdown>
![Current Flow](static/index/current-flow.png){ width="500" }
</figure>

**Proposal: Streamline communications**

<figure markdown>
![Proposed Flow](static/index/streamline.png){ width="500" }
</figure>




### **Service Overview**

![Service Overview](static/index/overview.png)

<figure markdown>
  ![Service Overview 2](static/index/overview-2.png){ width="400" }
</figure>


#### User configurations examples
Examples of possible user customized configurations for different channels:

- IT Status Board:
    - ```Important```: Push notifications during working hours
    - ```Normal```: Mail Live
    - ```Low```: Mail Daily Digest
- CERN News, Computing Blog:
    - Mail Daily/Weekly Digest
- Gate E Traffic:
    - Push notifications between 7am and 9am
- CERN IT Memes:
    - Disable notifications: web access only
- Indico:
    - Push notifications for direct notifications
  
### **Summary**
- Empower end users
    - Facilitate opt-in and opt-out
- Change information consumption
    - Step out of the Email get-it-all schema
    - Rely on more modern systems
- No similar product available on the market
    - Companies tend to regroup everything into one monolytic product for easier consumer lock-in
- **Notification service was designed with other systems integration in mind**
    - Acts as a hub between message systems.

